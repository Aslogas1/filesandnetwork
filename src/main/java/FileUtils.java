import java.io.File;
import java.util.Objects;

public class FileUtils {

    public static long calculateFolderSize(File directory) {
        long length = 0;
        for (File file : Objects.requireNonNull(directory.listFiles())) {
            if (file.isFile())
                length += file.length();
            else
                length += calculateFolderSize(file);
        }
        return length;
    }
}