import java.io.File;
import java.util.Scanner;

public class Main {

    private static final char[] sizeMultipliers = {'B', 'K', 'M', 'G', 'T'};

    public static void main(String[] args) {
        System.out.println("Введите путь до папки:");
        String scanner = new Scanner(System.in).nextLine();
        File file = new File(scanner);
        long l = FileUtils.calculateFolderSize(file);
        if(file.exists()) {
            System.out.println(getHumanReadableSize(l));
        } else {
            System.out.println("Файл не существует");
        }

    }
    public static String getHumanReadableSize(long size) {
        for (int i = 0; i < sizeMultipliers.length; i++) {
            double value = size / Math.pow(1024, i);
            if(value < 1024){
                return Math.round(value) + "" + sizeMultipliers[i] + (i > 0 ? "b" : "");
            }
        }
        return "Very big!";
    }
}
